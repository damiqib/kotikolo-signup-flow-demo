export interface IBuilding {
  name: string
  businessId: string
  buildingId: string
  type: string
  builtAt: number
  floors: number
  apartments: number
  heating: string
  lift: boolean
  sauna: boolean
}
