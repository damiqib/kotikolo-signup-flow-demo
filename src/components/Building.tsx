import { useState, forwardRef } from 'react'
import { Card, Button, Container, Row, Col } from 'react-bootstrap'

import buildingImage from '../assets/building.png'

import { IBuilding } from '../types/IBuilding'

type BuildingProps = {
  building: IBuilding
  nextPhase: () => void
  updatePrice: (sum: number) => void
}

const Building = forwardRef<HTMLDivElement, any>(
  ({ building, nextPhase, updatePrice }: BuildingProps, ref) => {
    const [completed, setCompleted] = useState<boolean>(false)

    const basePrice = building.apartments * 30

    return (
      <Container className="my-4">
        <Card>
          <Card.Header ref={ref}>Hakutulokset (näytetään 1/1)</Card.Header>
          <Card.Body>
            <Container>
              <Row>
                <Col sm={2}>
                  <Card.Img variant="top" src={buildingImage} />
                </Col>
                <Col>
                  <Card.Title>
                    {building.name} ({building.businessId})
                  </Card.Title>
                  <Card.Text>Rakennettu vuonna {building.builtAt}.</Card.Text>
                  <Card.Text>
                    Taloyhtiössä on {building.floors} kerrosta ja{' '}
                    {building.apartments} huoneistoa. Taloyhtiössä ei ole
                    hissiä, eikä liiketiloja.
                  </Card.Text>
                  <Card.Text className="emphased">
                    Peruspaketti sisältää kirjanpidon, tilinpäätöksen,
                    yhtiökokousasiakirjat, turvallinen asiakirjasäilytys sekä
                    mahdollisuuden manuaaliseen kulutusseurantaan.{' '}
                  </Card.Text>
                </Col>
                <Col sm={2} className="price">
                  Peruspaketti taloyhtiöllenne <br />
                  {basePrice} € / kk
                </Col>
              </Row>
            </Container>
            {!completed && (
              <Button
                variant="success"
                className="p-3 m-2 w-100"
                onClick={() => {
                  updatePrice(basePrice)
                  setCompleted(true)
                  nextPhase()
                }}
              >
                VALITSE {building.name}
              </Button>
            )}
          </Card.Body>
        </Card>
      </Container>
    )
  }
)

export { Building }
