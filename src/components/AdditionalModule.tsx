import { useState } from 'react'
import { Card, Button } from 'react-bootstrap'

import { IBuilding } from '../types/IBuilding'

type AdditionalModuleProps = {
  header: string
  title: string
  text: string
  price: number
  updatePrice: (sum: number) => void
  building: IBuilding
}

const AdditionalModule = ({
  header,
  title,
  text,
  price,
  updatePrice,
  building,
}: AdditionalModuleProps) => {
  const [selected, setSelected] = useState<null | boolean>(false)

  const toggleModule = () => {
    const s = !selected
    setSelected(s)
    updatePrice(s ? price * building.apartments : -price * building.apartments)
  }

  return (
    <Card>
      <Card.Header>{header}</Card.Header>
      <Card.Body>
        <Card.Title>{title}</Card.Title>
        <Card.Text>{text}</Card.Text>
        <Button
          variant={selected ? 'success' : 'secondary'}
          className="p-3 m-2 w-100"
          onClick={toggleModule}
        >
          {selected ? 'VALITTU' : 'VALITSE'}
        </Button>
      </Card.Body>
    </Card>
  )
}

export { AdditionalModule }
