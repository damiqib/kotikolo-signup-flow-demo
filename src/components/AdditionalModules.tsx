import { useState, forwardRef } from 'react'
import { Container, Card, Row, Col, Button } from 'react-bootstrap'

import { IBuilding } from '../types/IBuilding'

import { AdditionalModule } from './AdditionalModule'

type AdditionalModulesProps = {
  building: IBuilding
  nextPhase: () => void
  updatePrice: (sum: number) => void
}

const AdditionalModules = forwardRef<HTMLDivElement, any>(
  ({ building, nextPhase, updatePrice }: AdditionalModulesProps, ref) => {
    const [completed, setCompleted] = useState<boolean>(false)
    const [modulesPrice, setModulesPrice] = useState<number>(0)

    const updateModulePrice = (sum: number) => {
      setModulesPrice(modulesPrice + sum)
      updatePrice(sum)
    }

    return (
      <Container className="my-2">
        <Card>
          <Card.Header ref={ref}>Lisämoduulit</Card.Header>
          <Card.Body>
            <Container>
              <Row>
                <Col>
                  <p className="emphased">
                    Peruspaketin lisäksi voitte valita taloyhtiönne tarpeen
                    mukaisia lisäpalveluita.
                  </p>
                </Col>
              </Row>
              <Row>
                <Col>
                  <AdditionalModule
                    header="Taloyhtiön huoltokirja"
                    title="3 € / asunto / kk"
                    text="Mahdollistaa taloyhtiön huoltojen kirjaamisen ja seurannan."
                    price={3}
                    updatePrice={updateModulePrice}
                    building={building}
                  />
                </Col>
                <Col>
                  <AdditionalModule
                    header="Automaattinen kulutusseuranta"
                    title="3 € / asunto / kk"
                    text="Sähkön, lämmön sekä veden automaattinen kulutusseuranta sähkö/vesi/lämpö -yhtiöiden rajapintaratkaisuista"
                    price={3}
                    updatePrice={updateModulePrice}
                    building={building}
                  />
                </Col>
                <Col>
                  <AdditionalModule
                    header="Taloyhtiön oma ilmoitustaulu"
                    title="1 € / asunto / kk"
                    text="Sähköinen tiedotuskanava asukkaille"
                    price={1}
                    updatePrice={updateModulePrice}
                    building={building}
                  />
                </Col>
                <Col sm={2} className="price">
                  Valittujen lisämoduulien hinta <br /> {modulesPrice} € / kk
                </Col>
              </Row>
            </Container>
            {!completed && (
              <Button
                variant="success"
                className="p-3 m-2 w-100"
                onClick={() => {
                  setCompleted(true)
                  nextPhase()
                }}
              >
                JATKA
              </Button>
            )}
          </Card.Body>
        </Card>
      </Container>
    )
  }
)

export { AdditionalModules }
