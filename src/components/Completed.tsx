import networkVideo from '../assets/network.mp4'

const Completed = () => {
  return (
    <>
      <h1>Luodaan taloyhtiöllenne helpompaa isännöintikokemusta...</h1>

      <video autoPlay muted loop>
        <source src={networkVideo} type="video/mp4" />
      </video>
    </>
  )
}

export { Completed }
