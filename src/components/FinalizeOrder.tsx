import { forwardRef, useState, useEffect } from 'react'
import { Container, Card, Row, Col, Button } from 'react-bootstrap'

type FinalizeOrderProps = {
  nextPhase: () => void
  totalPrice: number
}

const FinalizeOrder = forwardRef<HTMLDivElement, any>(
  ({ nextPhase, totalPrice }: FinalizeOrderProps, ref) => {
    const [paymentType, setPaymentType] = useState<'monthly' | 'yearly' | null>(
      null
    )
    const [completed, setCompleted] = useState<boolean>(false)
    const [finalPrice, setFinalPrice] = useState<number>(totalPrice)

    useEffect(() => {
      if (paymentType === 'monthly') {
        setFinalPrice(totalPrice)
      }
      if (paymentType === 'yearly') {
        setFinalPrice(Math.floor(totalPrice * 0.9))
      }
    }, [paymentType, totalPrice])

    return (
      <>
        <Container className="my-2">
          <Card>
            <Card.Header ref={ref}>Tilausvahvistus</Card.Header>
            <Card.Body>
              <Container>
                <Row>
                  <Col>
                    <p>Valitkaa haluamanne laskutustapa.</p>
                  </Col>
                </Row>
                <Row>
                  <Col></Col>
                  <Col>
                    <Card>
                      <Card.Header>Kuukausiveloitteinen</Card.Header>
                      <Card.Body>
                        <Card.Text>
                          Kuukausiveloitteinen hinta taloyhtiöllenne on{' '}
                          {totalPrice} € / kk ({totalPrice * 12} € vuodessa).
                        </Card.Text>
                        <Button
                          variant={
                            paymentType === 'monthly' ? 'success' : 'secondary'
                          }
                          className="p-3 m-2 w-100"
                          onClick={() => {
                            setPaymentType('monthly')
                          }}
                        >
                          {paymentType === 'monthly' ? 'VALITTU' : 'VALITSE'}
                        </Button>
                      </Card.Body>
                    </Card>
                  </Col>
                  <Col>
                    <Card>
                      <Card.Header>Vuosiveloitteinen</Card.Header>
                      <Card.Body>
                        <Card.Text className="emphased">
                          Valitsemalla vuosittaisen etukäteislaskutuksen
                          säästätte 10 % kuukausimaksusta. Vuosihinta alennuksen
                          kanssa {Math.floor(totalPrice * 12 * 0.9)} €.
                        </Card.Text>
                        <Button
                          variant={
                            paymentType === 'yearly' ? 'success' : 'secondary'
                          }
                          className="p-3 m-2 w-100"
                          onClick={() => {
                            setPaymentType('yearly')
                          }}
                        >
                          {paymentType === 'yearly' ? 'VALITTU' : 'VALITSE'}
                        </Button>
                      </Card.Body>
                    </Card>
                  </Col>
                  <Col></Col>
                </Row>
              </Container>
              {!completed && (
                <Button
                  variant="info"
                  className="p-3 m-2 w-100"
                  style={{
                    color: 'white',
                    fontWeight: 'bolder',
                    fontSize: '1.4rem',
                  }}
                  onClick={() => {
                    setCompleted(true)
                    nextPhase()
                  }}
                  disabled={!paymentType}
                >
                  TILAA HELPOMPI ISÄNNÖINTI HINTAAN {finalPrice} € / kk
                </Button>
              )}
            </Card.Body>
          </Card>
        </Container>
      </>
    )
  }
)

export { FinalizeOrder }
