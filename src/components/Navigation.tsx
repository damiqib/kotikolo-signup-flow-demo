import { Container, Navbar, Nav, Button } from 'react-bootstrap'

const Navigation = () => {
  return (
    <Navbar
      bg="light"
      expand="lg"
      className="navbar navbar-expand-lg navbar-light bg-light shadow fixed-top"
    >
      <Container>
        <Navbar.Brand href="#">KOTIKOLO</Navbar.Brand>
        <Navbar.Collapse>
          <Nav className="me-auto"></Nav>
          <Nav>
            <Nav.Link href="#">Palvelut</Nav.Link>
            <Nav.Link href="#">Ota yhteyttä</Nav.Link>
          </Nav>
          <Button variant="outline-success">Kirjaudu</Button>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}

export { Navigation }
