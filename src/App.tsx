import { useState, useRef, useEffect } from 'react'
import { Container, Row, Form, FormControl, Button } from 'react-bootstrap'

import './App.css'

import { IBuilding } from './types/IBuilding'

import { Navigation } from './components/Navigation'
import { Building } from './components/Building'
import { AdditionalModules } from './components/AdditionalModules'
import { FinalizeOrder } from './components/FinalizeOrder'
import { Completed } from './components/Completed'

const App = () => {
  const [phase, setPhase] = useState<number>(0)
  const [totalPrice, setTotalPrice] = useState<number>(0)

  const buildingRef = useRef<null | HTMLDivElement>(null)
  const additionalModulesRef = useRef<null | HTMLDivElement>(null)
  const finalizeOrderRef = useRef<null | HTMLDivElement>(null)

  const demoBuilding: IBuilding = {
    name: 'Asunto Oy Koski-Kara',
    businessId: '0161640-5',
    buildingId: '102906202W',
    type: 'Kerrostalo',
    builtAt: 1968,
    floors: 5,
    apartments: 4,
    heating: 'Kaukolämpö',
    lift: true,
    sauna: true,
  }

  const nextPhase = () => {
    setPhase(phase + 1)
  }

  const updatePrice = (sum: number) => {
    setTotalPrice(totalPrice + sum)
  }

  useEffect(() => {
    if (phase === 1 && buildingRef.current) {
      buildingRef.current.scrollIntoView()
    }

    if (phase === 2 && additionalModulesRef.current) {
      additionalModulesRef.current.scrollIntoView()
    }

    if (phase === 3 && finalizeOrderRef.current) {
      finalizeOrderRef.current.scrollIntoView()
    }
  }, [phase])

  return (
    <Container style={{ width: '80%' }}>
      <Navigation />

      <Row style={{ marginTop: '56px' }}>
        <header>
          {phase < 4 ? (
            <>
              <h1>Helpompaa isännöintiä</h1>

              {/** In reality these would be fetched from DVV-api */}
              <Form className="d-flex mx-auto p-4" style={{ width: '500px' }}>
                <FormControl
                  type="search"
                  placeholder="Hae taloyhtiösi"
                  className="me-2 p-3"
                  aria-label="Etsi"
                />
                <Button variant="success" className="p-3" onClick={nextPhase}>
                  Hae
                </Button>
              </Form>
            </>
          ) : (
            <Completed />
          )}
        </header>
      </Row>

      <Row>
        {phase > 0 && phase < 4 && (
          <Building
            building={demoBuilding}
            nextPhase={nextPhase}
            updatePrice={updatePrice}
            ref={buildingRef}
          />
        )}
        {phase > 1 && phase < 4 && (
          <AdditionalModules
            building={demoBuilding}
            nextPhase={nextPhase}
            updatePrice={updatePrice}
            ref={additionalModulesRef}
          />
        )}
        {phase > 2 && phase < 4 && (
          <FinalizeOrder
            nextPhase={nextPhase}
            totalPrice={totalPrice}
            ref={finalizeOrderRef}
          />
        )}
      </Row>
    </Container>
  )
}

export { App }
