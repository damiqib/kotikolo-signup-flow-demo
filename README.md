# Kotikolo sign-up flow -demo

Sign-up demo flow for XAMK course "Sovelluskehitys liiketoimintana" group work (group 3) for a fictionary estate management platform.

## Live demo

The app is deployed to Heroku https://kotikolo-signup-flow-demo.herokuapp.com

## Tech stack

- Create React App with TypeScript template
- Gitlab CI-pipeline to Heroku

## Available commands

In the project directory, you can run:

`yarn start`

Runs the app in the development mode.
Open http://localhost:3000 to view it in the browser.
